package user

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"io"
	"os"
	"reflect"
)

type UserStruct struct {
	Prefix, Usr, UsrName     string
	UsrPasswd, UsrNamePasswd []byte
}

func InitNew(user *UserStruct) bool {
	err := os.MkdirAll(user.Prefix+user.Usr, 0766)
	if checkErr(err) {
		return false
	}

	return true
}

func GenerateKey(user *UserStruct) bool {
	bskey := user.Usr + string(user.UsrPasswd)
	var basekey []byte = []byte(bskey)
	var key []byte

	hasher := sha256.New()
	_, err := hasher.Write(basekey)
	if checkErr(err) {
		return false
	}
	key = hasher.Sum(nil)

	if err := os.WriteFile(user.Prefix+user.Usr+"/"+user.Usr+".key", key, 0755); err != nil {
		if checkErr(err) {
			return false
		}
	}

	return true
}

func AddPasswd(user *UserStruct) bool {
	err := os.MkdirAll(user.Prefix+user.Usr+"/"+user.UsrName, 0755)
	if checkErr(err) {
		return false
	}

	key, err := os.ReadFile(user.Prefix + user.Usr + "/" + user.Usr + ".key")
	if checkErr(err) {
		return false
	}

	ciph, err := aes.NewCipher(key)
	if checkErr(err) {
		return false
	}

	gcm, err := cipher.NewGCM(ciph)
	if checkErr(err) {
		return false
	}

	nonce := make([]byte, gcm.NonceSize())

	_, err = io.ReadFull(rand.Reader, nonce)
	if checkErr(err) {
		return false
	}

	if err := os.WriteFile(user.Prefix+user.Usr+"/"+user.UsrName+"/p.txt", gcm.Seal(nonce, nonce, user.UsrNamePasswd, nil), 0755); err != nil {
		if checkErr(err) {
			return false
		}
	}
	return true
}

func GetPasswd(user *UserStruct) string {
	if !Auth(user) {
		fmt.Println("Auth failed")
		os.Exit(1)
	}

	key, err := os.ReadFile(user.Prefix + user.Usr + "/" + user.Usr + ".key")
	if checkErr(err) {
		return ""
	}

	ciphertext, err := os.ReadFile(user.Prefix + user.Usr + "/" + user.UsrName + "/p.txt")
	if checkErr(err) {
		return ""
	}

	ciph, err := aes.NewCipher(key)
	if checkErr(err) {
		return ""
	}

	gcm, err := cipher.NewGCM(ciph)
	if checkErr(err) {
		return ""
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) > nonceSize {
		if checkErr(err) {
			return ""
		}
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if checkErr(err) {
		return ""
	}

	return string(plaintext)
}

func RemovePasswd(user *UserStruct) bool {
	if !Auth(user) {
		fmt.Println("Auth failed")
		os.Exit(1)
	}
	err := os.RemoveAll(user.Prefix + user.Usr + "/" + user.UsrName)
	if checkErr(err) {
		return false
	}
	return true
}

func Auth(user *UserStruct) bool {
	var basekey []byte = []byte(user.Usr + string(user.UsrPasswd))

	hasher := sha256.New()
	_, err := hasher.Write(basekey)
	if checkErr(err) {
		return false
	}
	testKey := hasher.Sum(nil)

	key, err := os.ReadFile(user.Prefix + user.Usr + "/" + user.Usr + ".key")
	if checkErr(err) {
		return false
	}

	if !reflect.DeepEqual(key, testKey) {
		fmt.Println("Incorrect password")
		os.Exit(1)
	}

	return true
}

func checkErr(err error) bool {
	if err != nil {
		return true;
	}
	return false
	// for debug
	// if err != nil {
	// 	fmt.Println(err)
	// }
}
