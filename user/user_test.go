package user

import (
	"os"
	"testing"
)

var testuser = UserStruct{
	Usr:           "test",
	UsrName:       "website",
	UsrPasswd:     []byte("out"),
	UsrNamePasswd: []byte("password"),
	Prefix:        "./",
}

func TestInit(t *testing.T) {
	InitNew(&testuser)

	newFile, err := os.Stat(testuser.Usr)
	checkErr(err)

	if !newFile.IsDir() {
		t.Error("No config file found.")
	}

	os.Remove(testuser.Usr)

}

func TestGenerateKey(t *testing.T) {
	InitNew(&testuser)
	GenerateKey(&testuser)

	newDir, err := os.Stat(testuser.Usr)
	checkErr(err)

	if !newDir.IsDir() {
		t.Error("No user folder found.")
	}

	_, err = os.Stat("./test/test.key")
	if err != nil {
		t.Error("No user folder found.")
	}

	os.RemoveAll(testuser.Usr)
}

func TestAddPasswd(t *testing.T) {

	InitNew(&testuser)
	GenerateKey(&testuser)

	newDir, err := os.Stat(testuser.Usr)
	checkErr(err)

	if !newDir.IsDir() {
		t.Error("No user folder found.")
	}

	_, err = os.Stat(testuser.Prefix + testuser.Usr + "/" + testuser.Usr + ".key")
	if err != nil {
		t.Error("No key file found found.")
	}

	AddPasswd(&testuser)

	_, err = os.Stat(testuser.Prefix + testuser.Usr + "/" + testuser.UsrName + "/p.txt")
	if err != nil {
		t.Error("No p.txt file found.")
	}

	os.RemoveAll(testuser.Usr)
}

func TestGetPasswd(t *testing.T) {

	InitNew(&testuser)
	GenerateKey(&testuser)

	newDir, err := os.Stat(testuser.Usr)
	checkErr(err)

	if !newDir.IsDir() {
		t.Error("No user folder found.")
	}

	_, err = os.Stat(testuser.Prefix + testuser.Usr + "/" + testuser.Usr + ".key")
	if err != nil {
		t.Error("No key file found found.")
	}

	AddPasswd(&testuser)

	_, err = os.Stat(testuser.Prefix + testuser.Usr + "/" + testuser.UsrName + "/p.txt")
	if err != nil {
		t.Error("No p.txt file found.")
	}

	var res string = GetPasswd(&testuser)

	if res != string(testuser.UsrNamePasswd) {
		t.Error("Incorrect/incorrectly parsed password returned.")
	}

	os.RemoveAll(testuser.Usr)
}

func TestRemovePasswd(t *testing.T) {

	InitNew(&testuser)
	GenerateKey(&testuser)

	newDir, err := os.Stat(testuser.Usr)
	checkErr(err)

	if !newDir.IsDir() {
		t.Error("No user folder found.")
	}

	_, err = os.Stat(testuser.Prefix + testuser.Usr + "/" + testuser.Usr + ".key")
	if err != nil {
		t.Error("No key file found found.")
	}

	AddPasswd(&testuser)

	_, err = os.Stat(testuser.Prefix + testuser.Usr + "/" + testuser.UsrName + "/p.txt")
	if err != nil {
		t.Error("No p.txt file found.")
	}

	RemovePasswd(&testuser)

	_, err = os.Stat(testuser.Prefix + testuser.Usr + "/" + testuser.UsrName)
	if err == nil {
		t.Error("Folder for password not deleted.")
	}

	os.RemoveAll(testuser.Usr)
}
