package main

import (
	"fmt"
	"golang.org/x/term"
	"os"
	"passmgr/user"
	"syscall"
)

func help() {
	fmt.Println(
		"Help:\n",
		"Setup: passmgr init\n",
		"New Password: passmgr -a <folder>/<username>\n",
		"Access Password: passmgr <folder>/<username>\n",
		"Remove Password: passmgr -r <folder>/<username>",
	)
}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func initUser(currentUser *user.UserStruct) {
	var err error
	fmt.Print("Enter new user: ")
	fmt.Scanln(&currentUser.Usr)

	fmt.Print("Enter new password: ")
	currentUser.UsrPasswd, err = term.ReadPassword(int(syscall.Stdin))
	checkErr(err)
	fmt.Print("\n")

	fmt.Print("Verify password: ")
	vpasswd, err := term.ReadPassword(int(syscall.Stdin))
	checkErr(err)
	fmt.Print("\n")

	for string(currentUser.UsrPasswd) != string(vpasswd) {
		fmt.Print("Enter new password: ")
		currentUser.UsrPasswd, err = term.ReadPassword(int(syscall.Stdin))
		checkErr(err)
		fmt.Print("\n")

		fmt.Print("Verify password: ")
		vpasswd, err = term.ReadPassword(int(syscall.Stdin))
		checkErr(err)
		fmt.Print("\n")
	}

	if !user.InitNew(currentUser) {
		fmt.Println("User initialization failed")
		os.Exit(1)
	}
	if !user.GenerateKey(currentUser) {
		fmt.Println("Key generation failed")
		os.Exit(1)
	}
}

func addPasswd(currentUser *user.UserStruct) {
	var err error
	fmt.Print("Enter user: ")
	fmt.Scanln(&currentUser.Usr)

	fmt.Print("Enter new account/username: ")
	fmt.Scanln(&currentUser.UsrName)

	fmt.Printf("Enter password for %s: ", currentUser.UsrName)
	currentUser.UsrNamePasswd, err = term.ReadPassword(int(syscall.Stdin))
	checkErr(err)
	fmt.Print("\n")

	fmt.Printf("Verify password for %s: ", currentUser.UsrName)
	vpasswd, err := term.ReadPassword(int(syscall.Stdin))
	checkErr(err)
	fmt.Print("\n")

	for string(currentUser.UsrNamePasswd) != string(vpasswd) {
		fmt.Println("Passwords did not match!")
		fmt.Print("Enter password again: ")
		currentUser.UsrNamePasswd, err = term.ReadPassword(int(syscall.Stdin))
		checkErr(err)
		fmt.Print("\n")
		fmt.Print("Verify password: ")
		vpasswd, err = term.ReadPassword(int(syscall.Stdin))
		checkErr(err)
		fmt.Print("\n")
	}

	if !user.AddPasswd(currentUser) {
		fmt.Println("Adding password failed")
		os.Exit(1)
	}
}

func removePasswd(currentUser *user.UserStruct) {
	var err error
	fmt.Print("Enter user: ")
	fmt.Scanln(&currentUser.Usr)

	fmt.Printf("Enter password for %s: ", currentUser.Usr)
	currentUser.UsrPasswd, err = term.ReadPassword(int(syscall.Stdin))
	checkErr(err)
	fmt.Print("\n")

	fmt.Printf("Enter account to delete for %s: ", currentUser.UsrName)
	fmt.Scanln(&currentUser.UsrName)

	if !user.RemovePasswd(currentUser) {
		fmt.Println("Password removal failed")
		os.Exit(1)
	}
}

func retrievePasswd(currentUser *user.UserStruct) {
	var err error
	fmt.Print("Enter username: ")
	fmt.Scanln(&currentUser.Usr)

	fmt.Printf("Enter password for user %s: ", currentUser.Usr)
	currentUser.UsrPasswd, err = term.ReadPassword(int(syscall.Stdin))
	checkErr(err)
	fmt.Print("\n")

	currentUser.UsrName = os.Args[1]

	if "" == user.GetPasswd(currentUser) {
		fmt.Println("Password retreval error")
		os.Exit(1)
	} else {
		fmt.Println(user.GetPasswd(currentUser))
	}

}

func main() {

	if len(os.Args) < 2 || os.Args[1] == "-h" || os.Args[1] == "--help" {
		help()
	}

	home, err := os.UserHomeDir()
	checkErr(err)

	_, err = os.Stat(home + "/.config")
	checkErr(err)

	currentUser := user.UserStruct{
		Prefix: home + "/.config/passmgr/",
	}

	switch os.Args[1] {
	case "-a":
		addPasswd(&currentUser)
	case "-r":
		removePasswd(&currentUser)
	case "init":
		initUser(&currentUser)
	default:
		retrievePasswd(&currentUser)
	}

}
