# Password Manager (passmgr)

## Goal:

Crate a password manager similar to 'pass' from the *nix world. Allow for password
storage as files in directory system. Handle authentication of user as well.

## Status:

At this point, the product is stable and feature complete. It allows for user
creation, authentication, and encrypted password storage with decryption upon
password retrevial.

Complex naming is also available. Thus a user with mulitple email accounts could
do something such as create a password stored as email/yahoo and another as
email/gmail and could access them as such. This allows for clean file system storage.
